import Title from "@/components/title";
import Content from "@/components/content";
import Project from "@/components/project";

import translate from "@/locales/translate";

import projects from "@/content/projects";

export default async function Projects(props: {
  params: Promise<{ locale: "de" | "en" }>;
}) {
  const params = await props.params;

  const { locale } = params;

  const t = translate(locale);

  return (
    <div className="w-full">
      <Title>{t("projects")}</Title>

      <Content showLines={false}>
        {projects.map((project, i) => (
          <Project
            project={project}
            shorten={true}
            locale={locale}
            t={t}
            key={i}
          />
        ))}
      </Content>
    </div>
  );
}
