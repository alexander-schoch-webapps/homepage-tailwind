import { notFound } from "next/navigation";

import parse from "html-react-parser";

import Title from "@/components/title";
import Content from "@/components/content";
import Project from "@/components/project";

import translate from "@/locales/translate";

import projects from "@/content/projects";

export default async function ProjectPage(props: {
  params: Promise<{ locale: "de" | "en"; slug: string }>;
}) {
  const params = await props.params;

  const { locale, slug } = params;

  const t = translate(locale);

  const matches = projects.filter((p) => p.slug === slug);

  if (matches.length === 0) notFound();

  const project = matches[0];

  return (
    <div className="flex w-full grow flex-col">
      <Title subtitle={parse(project.title[locale])}>{t("project")}</Title>

      <Content showLines={false}>
        <Project project={project} shorten={false} locale={locale} t={t} />
      </Content>
    </div>
  );
}
