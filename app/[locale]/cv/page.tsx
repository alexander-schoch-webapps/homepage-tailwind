import {
  IconDownload,
  IconSchool,
  IconBuildingFactory2,
  IconExternalLink,
  IconInfoCircle,
} from "@tabler/icons-react";

import Title from "@/components/title";
import Content from "@/components/content";
import VSETHLogo from "@/components/vsethLogo";

import translate from "@/locales/translate";

import CV_de from "@/content/cv/cv.de.json";
import CV_en from "@/content/cv/cv.en.json";
import VSETH_de from "@/content/cv/vseth.de.json";
import VSETH_en from "@/content/cv/vseth.en.json";

export default async function CV(props: {
  params: Promise<{ locale: string }>;
}) {
  const params = await props.params;

  const { locale } = params;

  const t = translate(locale);
  const VSETH = locale === "de" ? VSETH_de : VSETH_en;
  const CV = locale === "de" ? CV_de : CV_en;

  return (
    <div className="w-full">
      <Title>{t("cv")}</Title>

      <Content showLines={false}>
        <div className="mb-8 grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div className="hidden flex-col justify-between sm:flex">
            <div />
            <div className="mx-auto w-fit rounded-full bg-gradient-to-r from-purple-600 to-pink-600 p-4 shadow">
              <IconSchool size={80} className="text-white" />
            </div>
            <div className="border-b-2 border-b-purple-600 text-right dark:border-b-white">
              <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
                {t("education")}
              </h2>
            </div>
          </div>
          <div className="block sm:hidden">
            <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
              {t("education")}
            </h2>
          </div>

          <ol className="relative mt-4 border-l border-purple-600 dark:border-white">
            {CV.education.map((entry, i) => (
              <li className="mb-10 ms-4" key={i}>
                <div className="absolute -start-1.5 mt-1.5 h-3 w-3 rounded-full border border-white bg-purple-600 dark:border-gray-900 dark:bg-white"></div>
                <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-300">{`${entry.start} − ${entry.end}`}</time>
                <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                  {entry.name}
                </h3>
                <p className="text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.institution}
                </p>
                <p className="mb-4 text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.address}
                </p>
              </li>
            ))}
          </ol>
        </div>

        <div className="mb-8 mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div className="hidden flex-col justify-between sm:flex">
            <div />
            <div className="mx-auto w-fit rounded-full bg-gradient-to-r from-purple-600 to-pink-600 p-4 shadow">
              <IconBuildingFactory2 size={80} className="text-white" />
            </div>
            <div className="border-b-2 border-b-purple-600 text-right dark:border-b-white">
              <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
                {t("work")}
              </h2>
            </div>
          </div>
          <div className="block sm:hidden">
            <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
              {t("work")}
            </h2>
          </div>

          <ol className="relative mt-4 border-l border-purple-600 dark:border-white">
            {CV.work.map((entry, i) => (
              <li className="mb-10 ms-4" key={i}>
                <div className="absolute -start-1.5 mt-1.5 h-3 w-3 rounded-full border border-white bg-purple-600 dark:border-gray-900 dark:bg-white"></div>
                <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-300">
                  {entry.start === entry.end
                    ? entry.start
                    : `${entry.start} − ${entry.end}`}
                </time>
                <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                  {entry.employer}
                </h3>
                <p className="mb-2 text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.job}
                </p>
                <p className="mb-2 text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.description}
                </p>
                {entry.scripts && (
                  <div>
                    {entry.scripts.map((script, i) => (
                      <a href={script.url} target="_blank" key={i}>
                        <button className="mb-2 mr-2 inline-flex items-center rounded-md bg-purple-600 px-3 py-2 text-sm font-semibold text-white shadow">
                          <IconDownload className="mr-2" size={18} />
                          <span>{script.title}</span>
                        </button>
                      </a>
                    ))}
                  </div>
                )}
                {entry.links && (
                  <div>
                    {entry.links.map((link, i) => (
                      <a href={link.url} target="_blank" key={i}>
                        <button className="mb-2 mr-2 inline-flex items-center rounded-md bg-purple-600 px-3 py-2 text-sm font-semibold text-white shadow">
                          <IconExternalLink className="mr-2" size={18} />
                          <span>{link.title}</span>
                        </button>
                      </a>
                    ))}
                  </div>
                )}
              </li>
            ))}
          </ol>
        </div>

        <div className="mb-8 mt-4 grid grid-cols-1 gap-4 sm:grid-cols-2">
          <div className="hidden flex-col justify-between sm:flex">
            <div />
            <div className="mx-auto w-fit rounded-full bg-gradient-to-r from-purple-600 to-pink-600 p-4 shadow">
              <VSETHLogo size={80} className="fill-white" />
            </div>
            <div className="border-b-2 border-b-purple-600 text-right dark:border-b-white">
              <div className="mb-4 rounded-md bg-purple-200 p-4 dark:bg-stone-700">
                <div className="flex text-left">
                  <IconInfoCircle
                    size={16}
                    className="mr-6 h-8 w-16 text-purple-600 dark:text-white"
                  />
                  <div>
                    <p className="mb-2 font-bold text-black dark:text-stone-100">
                      VSETH
                    </p>
                    <p className="text-black dark:text-stone-100">
                      {VSETH.description}
                    </p>
                  </div>
                </div>
              </div>
              <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
                {t("vseth")}
              </h2>
            </div>
          </div>
          <div className="block sm:hidden">
            <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
              {t("vseth")}
            </h2>
          </div>

          <ol className="relative mt-4 border-l border-purple-600 dark:border-white">
            {VSETH.positions.map((entry, i) => (
              <li className="mb-10 ms-4" key={i}>
                <div className="absolute -start-1.5 mt-1.5 h-3 w-3 rounded-full border border-white bg-purple-600 dark:border-gray-900 dark:bg-white"></div>
                <time className="mb-1 text-sm font-normal leading-none text-gray-400 dark:text-gray-300">
                  {entry.start === entry.end
                    ? entry.start
                    : `${entry.start} − ${entry.end}`}
                </time>
                <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
                  {entry.employer}
                </h3>
                <p className="mb-2 text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.job}
                </p>
                <p className="mb-4 text-base font-normal text-gray-500 dark:text-gray-300">
                  {entry.description}
                </p>
                {entry.website && (
                  <div>
                    <a href={entry.website} target="_blank">
                      <button className="mr-2 inline-flex items-center rounded-md bg-purple-600 px-3 py-2 text-sm font-semibold text-white shadow">
                        <IconExternalLink className="mr-2" size={18} />
                        <span>{t("website")}</span>
                      </button>
                    </a>
                  </div>
                )}
              </li>
            ))}
          </ol>
        </div>
      </Content>
    </div>
  );
}
