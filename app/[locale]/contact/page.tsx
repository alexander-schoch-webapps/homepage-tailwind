import Title from "@/components/title";
import Content from "@/components/content";
import ContactForm from "@/components/contactForm";

import translate from "@/locales/translate";

export default async function Contact(props: {
  params: Promise<{ locale: string; slug: string }>;
}) {
  const params = await props.params;

  const { locale, slug } = params;

  const t = translate(locale);

  return (
    <div className="flex w-full grow flex-col">
      <Title>{t("contactMe")}</Title>

      <Content>
        <ContactForm locale={locale} />
      </Content>
    </div>
  );
}
