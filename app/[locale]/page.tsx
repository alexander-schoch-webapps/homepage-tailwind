import Image from "next/image";

import Header from "@/components/header";
import Tagline from "@/components/tagline";
import About from "@/components/about";
import Content from "@/components/content";
import ApplicationCard from "@/components/applicationCard";
import BlogPosts from "@/components/blogPosts";
import Mastodon from "@/components/mastodon";

import translate from "@/locales/translate";

import applications_de from "@/content/applications.de.json";
import applications_en from "@/content/applications.en.json";

export default async function Home(props: {
  params: Promise<{ locale: "de" | "en" }>;
}) {
  const params = await props.params;

  const { locale } = params;

  const t = translate(locale);
  const applications = locale === "de" ? applications_de : applications_en;

  return (
    <div className="w-full">
      <Header t={t} locale={locale} />

      <section id="content" />
      <Tagline locale={locale} />
      <About t={t} locale={locale} />

      <Content>
        <div className="mb-8">
          <h2 className="inline text-4xl font-black text-purple-600">
            {t("blogPosts")}
          </h2>

          <BlogPosts locale={locale} />
        </div>
      </Content>

      <Content fullWidth={true}>
        <div className="mb-8">
          <div className="mx-auto w-[80rem] max-w-full">
            <h2 className="inline text-4xl font-black text-purple-600">
              {t("mastodon")}
            </h2>
          </div>

          <Mastodon locale={locale} />
        </div>
      </Content>

      <Content>
        <div className="mb-8">
          <h2 className="inline text-4xl font-black text-purple-600">
            {t("myApplications")}
          </h2>
        </div>
        <div className="columns-1 gap-4 sm:columns-2 lg:columns-3">
          {applications.map((app) => (
            <ApplicationCard key={app.name} app={app} t={t} />
          ))}
        </div>
      </Content>
    </div>
  );
}
