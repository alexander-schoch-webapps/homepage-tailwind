import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "@/app/globals.css";

const inter = Inter({ subsets: ["latin"] });

import Navbar from "@/components/navbar";
import Matomo from "@/components/matomo";

export const metadata: Metadata = {
  title: "Alexander Schoch",
  description:
    "Homepage of Alexander Schoch, chemical engineer and full stack web developer based in Zurich, Switzerland",
  metadataBase: new URL("https://aschoch.ch"),
  alternates: {
    canonical: "/",
    languages: {
      "en-GB": "/en",
      "de-CH": "/de",
    },
  },
  openGraph: {
    images: "/og-image.png",
  },
};

export default async function RootLayout(props: {
  children: React.ReactNode;
  params: Promise<{ locale: "de" | "en" }>;
}) {
  const params = await props.params;

  const { locale } = params;

  const { children } = props;

  return (
    <html lang={locale} className="scroll-smooth">
      <head>
        <link rel="icon" href="/favicon.svg" sizes="any" />
        <title>Alexander Schoch</title>
      </head>
      <body
        className={`${inter.className} min-h-screen text-black dark:text-stone-100`}
      >
        <div
          className="bg-grid-slate-900 fixed inset-0 z-0 block h-screen bg-white dark:hidden"
          style={{
            backgroundSize: "10px 10px",
            backgroundImage:
              "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to top, transparent, black)",
            WebkitMaskImage: "linear-gradient(to top, transparent, black)",
          }}
        />
        <div
          className="bg-grid-slate-900 fixed inset-0 z-0 hidden h-screen bg-gradient-to-r from-violet-900 to-pink-900 dark:block"
          style={{
            backgroundSize: "20px 20px",
            backgroundImage:
              "radial-gradient(circle, #777777 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to top, transparent, black)",
            WebkitMaskImage: "linear-gradient(to top, transparent, black)",
          }}
        />
        <Navbar locale={locale}>{children}</Navbar>
        <Matomo />
      </body>
    </html>
  );
}
