import { type NextRequest } from "next/server";
import nodemailer from "nodemailer";

type Data = {
  name: string;
};

export async function POST(request: NextRequest) {
  const { name, mail, message, human, robot } = await request.json();

  if (
    human == true &&
    robot == false &&
    name != "" &&
    mail != "" &&
    message != ""
  ) {
    const transporter = nodemailer.createTransport({
      host: process.env.MAILER_HOST,
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: process.env.MAILER_USERNAME,
        pass: process.env.MAILER_PASSWORD,
      },
    });

    await transporter.sendMail({
      from: process.env.MAILER_ADDRESS,
      to: process.env.MAILER_ADDRESS,
      replyTo: mail,
      subject: (process.env.MAILER_SUBJECT || "").replace("NAME", name),
      html: message,
    });

    return new Response("mailSuccess");
  } else {
    return new Response("mailFailure");
  }
}
