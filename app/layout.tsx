import { ReactNode } from "react";

type Props = {
  children: ReactNode;
};

export const metadata = {
  metadataBase: new URL("https://aschoch.ch"),
  alternates: {
    canonical: "/",
    languages: {
      "en-GB": "/en",
      "de-CH": "/de",
    },
  },
  openGraph: {
    images: "/og-image.png",
  },
};

// Even though this component is just passing its children through, the presence
// of this file fixes an issue in Next.js 13.4 where link clicks that switch
// the locale would otherwise cause a full reload.
export default function RootLayout({ children }: Props) {
  return <>{children}</>;
}
