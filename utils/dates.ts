import dayjs from "dayjs";

import relativeTime from "dayjs/plugin/relativeTime";
import localizedFormat from "dayjs/plugin/localizedFormat";
import "dayjs/locale/de-ch";
import "dayjs/locale/en-gb";
dayjs.extend(relativeTime);
dayjs.extend(localizedFormat);

export function formatDate(dateStr: string | Date, locale: string) {
  const localeMapping = {
    de: "de-ch",
    en: "en-gb",
  };
  const fullLocale = locale ? localeMapping[locale as "de" | "en"] : "en-gb";
  dayjs.locale(fullLocale);

  return dayjs().to(dayjs(dateStr));
}
