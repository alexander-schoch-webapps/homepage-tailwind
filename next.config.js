/** @type {import('next').NextConfig} */
const nextConfig = {
  rewrites: async () => {
    return [
      {
        source: "/feed.xml",
        destination: "https://blog.aschoch.ch/feed.xml",
      },
      {
        source: "/api/comments/:id*",
        destination: "https://blog.aschoch.ch/api/comments/:id*",
      },
    ];
  },
};

module.exports = nextConfig;
