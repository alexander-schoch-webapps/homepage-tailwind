"use client";

import { useState, useEffect } from "react";

import { IconArrowRight, IconMessage2 } from "@tabler/icons-react";

import Parser from "rss-parser";
const parser = new Parser();

import { formatDate } from "@/utils/dates";

interface BlogPostType {
  creator?: string | undefined;
  title?: string | undefined;
  isoDate?: string | undefined;
  link?: string | undefined;
  contentSnippet?: string | undefined;
  categories?: string[] | undefined;
}

interface CommentNumberType {
  [id: string]: number;
}

export default function BlogPosts({ locale }: { locale: string }) {
  const [data, setData] = useState<BlogPostType[]>([]);
  const [comments, setComments] = useState<CommentNumberType>({});
  const RSS_URL = "/feed.xml";

  const lang_tag = locale === "de" ? "german" : "english";

  useEffect(() => {
    const fetch = async () => {
      const feed = await parser.parseURL(RSS_URL);
      setData(
        feed.items.filter((i) => i.categories?.includes(lang_tag) || false),
      );
    };
    fetch();
  }, []);

  useEffect(() => {
    (async () => {
      const comments = await Promise.all(
        data.map(async (d) => {
          const id = d.link!.split("/")[d.link!.split("/").length - 1];
          const cmt = await fetch(`/api/comments/${id}`);
          const data = await cmt.json();
          return {
            id: d.link!,
            comments: data.length,
          };
        }),
      );
      const cmtObj = comments.reduce(
        (acc, val) => ({ ...acc, [val["id"]]: val["comments"] }),
        {},
      );
      setComments(cmtObj);
    })();
  }, [data]);

  if (data.length === 0) return <></>;

  return (
    <div className="relative z-20 my-4 grid grid-cols-1 gap-4 text-black sm:grid-cols-2 dark:text-white">
      {data.map((d, i) => (
        <div
          className="relative z-10 mb-4 flex break-inside-avoid-column flex-col justify-between rounded-xl border border-purple-200 bg-white p-4 shadow-md dark:border-stone-600 dark:bg-stone-700"
          key={i}
        >
          <a target="_blank" href={`https://blog.aschoch.ch${d.link}`}>
            <h2 className="mb-2 text-xl font-semibold text-purple-600 dark:text-white">
              {d.title}
            </h2>
          </a>
          <div className="mb-1 flex items-center gap-2">
            <span className="inline-flex rounded-full bg-stone-200 px-4 py-[2px] text-xs font-semibold uppercase text-purple-700 dark:bg-stone-500 dark:text-white">
              {d.creator}
            </span>
            {d.isoDate && <span>{formatDate(d.isoDate, locale)}</span>}
          </div>
          {d.categories && (
            <div className="my-1 flex flex-wrap items-center">
              {d.categories.map((c, i) => (
                <a
                  className="mb-1 mr-2 inline-flex whitespace-nowrap rounded-full bg-purple-100 px-4 py-[2px] text-xs font-semibold uppercase text-purple-700 dark:bg-violet-600 dark:text-white"
                  key={i}
                  target="_blank"
                  href={`https://blog.aschoch.ch/tags/${c}`}
                >
                  {c}
                </a>
              ))}
            </div>
          )}
          <p className="py-2">
            <span>{d.contentSnippet}</span>
            <a
              className="ml-4 underline"
              href={`https://blog.aschoch.ch${d.link}`}
              target="_blank"
            >
              Read More →
            </a>
          </p>
          {Object.keys(comments).length > 0 && (
            <div>
              <a
                href={`https://blog.aschoch.ch${d.link}#comments`}
                target="_blank"
                className="flex w-fit items-center gap-2 rounded-md px-4 py-2 hover:bg-gray-100 dark:hover:bg-stone-600"
              >
                <IconMessage2 />
                <span>{comments[d.link!]}</span>
              </a>
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
