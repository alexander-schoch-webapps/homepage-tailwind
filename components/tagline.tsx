"use client";

import Link from "next/link";

import { useState } from "react";

import { IconArrowRight } from "@tabler/icons-react";

import translate from "@/locales/translate";

import taglineDataEN from "@/content/tagline/tagline_en";
import taglineDataDE from "@/content/tagline/tagline_de";

import TaglineCarousel from "@/components/taglineCarousel";

export default function Tagline({ locale }: { locale: "de" | "en" }) {
  const t = translate(locale);
  const taglineData = locale === "en" ? taglineDataEN : taglineDataDE;
  const [slide, setSlide] = useState<number>(0);

  const colors = [
    "blue",
    "indigo",
    "violet",
    "purple",
    "fuchsia",
    "pink",
    "rose",
    "red",
  ];

  const englishKeys = [
    "user-friendly",
    "beautiful",
    "fast",
    "secure",
    "accessible",
    "reliable",
    "maintainable",
    "open source",
  ];

  const germanKeys = [
    "benutzerfreundlich",
    "hübsch",
    "schnell",
    "sicher",
    "barrierefrei",
    "zuverlässig",
    "wartbar",
    "quelloffen",
  ];

  const keys = locale === "en" ? englishKeys : germanKeys;

  const colorClasses = colors.map((c) => `text-${c}-600 dark:text-${c}-500`);
  const combinedColors = colorClasses.join(" ");

  return (
    <div className="relative flex justify-center border-t border-stone-200 bg-red-50 px-4 pb-4 dark:border-stone-700 dark:bg-stone-800">
      <div className="absolute z-0 mx-auto hidden h-full w-[80rem] max-w-full sm:block">
        <div
          className="absolute right-24 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
          style={{
            backgroundSize: "2px 13px",
            backgroundImage:
              "linear-gradient(rgb(220, 38, 38) 61%,rgba(255,255,255,0) 0%)",
          }}
        />
        <div
          className="absolute right-32 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
          style={{
            backgroundSize: "2px 13px",
            backgroundImage:
              "linear-gradient(rgb(220, 38, 38) 61%,rgba(255,255,255,0) 0%)",
          }}
        />
      </div>
      <div className="z-10 mx-auto w-[80rem] max-w-full py-16 text-center">
        <div>
          <div className="text-blue-600 text-fuchsia-600 text-indigo-600 text-pink-600 text-purple-600 text-red-600 text-rose-600 text-violet-600 dark:text-blue-500 dark:text-fuchsia-500 dark:text-indigo-500 dark:text-pink-500 dark:text-purple-500 dark:text-red-500 dark:text-rose-500 dark:text-violet-500" />
          <div className="mb-8">
            <h2 className="inline bg-red-50 text-3xl font-bold leading-tight text-black sm:text-5xl sm:leading-tight dark:bg-stone-800 dark:text-stone-100">
              {t("inNeedOfA")}{" "}
              {keys.map((k, i) => (
                <span key={k}>
                  <span
                    className={`font-black ${colorClasses[i]}`}
                    onMouseEnter={() => {
                      setSlide(i);
                    }}
                  >{`${k}${locale === "de" ? "e" : ""}`}</span>
                  {i < keys.length - 2 ? ", " : ""}
                  {i === keys.length - 2 ? ` ${t("and")} ` : ""}
                </span>
              ))}{" "}
              {t("webApplication")}?
            </h2>
          </div>
        </div>
        <div className="mx-auto w-[80rem] max-w-full">
          <TaglineCarousel
            slide={slide}
            setSlide={(i: number) => {
              setSlide(i);
            }}
            content={taglineData}
            colors={colorClasses}
            keys={keys}
          />
        </div>
        <div className="mx-auto mt-8 w-[80rem] max-w-full">
          <Link
            href="/contact"
            className="mr-2 inline-flex items-center rounded-xl bg-gradient-to-r from-purple-600 to-pink-600 px-4 py-3 text-2xl font-bold text-white shadow-xl"
          >
            {t("contactMe")}
            <IconArrowRight className="ml-2" size={32} />
          </Link>
        </div>
      </div>
    </div>
  );
}
