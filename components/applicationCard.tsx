import { IconCode, IconExternalLink } from "@tabler/icons-react";

interface ApplicationType {
  name: string;
  url: string;
  gitlab: string;
  description: string;
  tags: string[];
}

export default function ApplicationCard({
  app,
  t,
}: {
  app: ApplicationType;
  t: (key: string) => string;
}) {
  return (
    <div className="relative z-10 mb-4 flex break-inside-avoid-column flex-col justify-between rounded-xl border border-purple-200 bg-white shadow-md dark:border-stone-600 dark:bg-stone-700">
      <div className="pl-4 pr-4 pt-4">
        <h2 className="mb-2 text-xl font-semibold text-purple-600 dark:text-white">
          {app.name}
        </h2>
        <p className="py-2 text-black dark:text-stone-100">{app.description}</p>
      </div>
      <div className="relative">
        <div
          className="bg-grid-slate-900 absolute inset-0 z-20 block h-full dark:hidden"
          style={{
            backgroundSize: "10px 10px",
            backgroundImage:
              "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />
        <div
          className="bg-grid-slate-900 absolute inset-0 z-20 hidden h-full dark:block"
          style={{
            backgroundSize: "10px 10px",
            backgroundImage:
              "radial-gradient(circle, #555555 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />
        <div className="relative z-30 pb-4 pl-4 pr-4">
          <div className="mb-2">
            {app.tags.map((tag) => (
              <span
                key={tag}
                className="mb-1 mr-2 inline-flex rounded-full bg-purple-100 px-4 py-1 text-xs font-semibold uppercase text-purple-700 shadow-md dark:bg-violet-600 dark:text-white"
              >
                {tag}
              </span>
            ))}
          </div>
          <a href={app.url} target="_blank">
            <button className="mr-2 inline-flex items-center rounded-md bg-gradient-to-r from-purple-600 to-pink-600 px-3 py-2 text-sm font-semibold text-white shadow">
              <IconExternalLink className="mr-2" size={18} />
              {t("tryIt")}
            </button>
          </a>
          <a href={app.gitlab} target="_blank">
            <button className="inline-flex items-center rounded-md border border-purple-600 bg-white px-3 py-2 text-sm font-semibold text-purple-600 shadow">
              <IconCode className="mr-2" size={18} />
              <span>{t("code")}</span>
            </button>
          </a>
        </div>
      </div>
    </div>
  );
}
