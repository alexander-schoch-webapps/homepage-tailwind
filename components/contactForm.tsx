"use client";

import { useState, FormEvent } from "react";

import { IconSend2 } from "@tabler/icons-react";

import translate from "@/locales/translate";

import axios from "axios";

export default function ContactForm({ locale }: { locale: string }) {
  const [name, setName] = useState<string>("");
  const [mail, setMail] = useState<string>("");
  const [message, setMessage] = useState<string>("");
  const [human, setHuman] = useState<boolean>(false);
  const [robot, setRobot] = useState<boolean>(false);

  const [title, setTitle] = useState<string>("");
  const [msg, setMsg] = useState<string>("");
  const [color, setColor] = useState<string>("red");

  const [isOpen, setIsOpen] = useState<boolean>(false);

  const t = translate(locale);

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (!validate()) {
      notification(t("formIncomplete"), t("formIncompleteText"), "red");
      return;
    }

    const response = await axios.post("/api/contact", {
      name: name,
      mail: mail,
      message: message,
      human: human,
      robot: robot,
    });

    if (response.data == "mailSuccess") {
      notification(t("mailSuccess"), t("mailSuccessText"), "green");
    } else {
      notification(t("mailFailure"), t("mailFailureText"), "red");
    }
  };

  const notification = (title: string, message: string, color: string) => {
    setTitle(title);
    setMsg(message);
    setColor(color);

    setIsOpen(true);
    const interval = setTimeout(() => {
      setIsOpen(false);
    }, 5000);
  };

  const validate = () => {
    if (name == "") return false;
    if (mail == "") return false;
    if (message == "") return false;
    return true;
  };

  return (
    <>
      <form>
        <noscript>This from requires Javascript to work.</noscript>
        <div className="mb-4 sm:grid sm:grid-cols-2 sm:gap-4">
          <div>
            <label className="block w-full" htmlFor="username">
              <span className="text-sm font-bold text-gray-700 dark:text-stone-300">
                Name
              </span>
              <input
                value={name}
                onChange={(e) => setName(e.target.value)}
                className="focus:shadow-outline relative z-10 w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none dark:border-stone-600 dark:bg-stone-700 dark:text-white"
                id="username"
                type="text"
                placeholder="Maxime Musterfrau"
              />
            </label>
          </div>
          <div className="mt-4 sm:mt-0">
            <label className="mb-2 block" htmlFor="email">
              <span className="text-sm font-bold text-gray-700 dark:text-stone-300">
                {t("email")}
              </span>
              <input
                value={mail}
                onChange={(e) => setMail(e.target.value)}
                className="focus:shadow-outline relative z-10 w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none dark:border-stone-600 dark:bg-stone-700 dark:text-white"
                id="email"
                type="text"
                placeholder="maxime@musterfrau.ch"
              />
            </label>
          </div>
          <div className="col-span-2 mt-4 sm:mt-0">
            <label className="mb-2 block" htmlFor="message">
              <span className="text-sm font-bold text-gray-700 dark:text-stone-300">
                {t("message")}
              </span>
              <textarea
                value={message}
                onChange={(e) => setMessage(e.target.value)}
                rows={5}
                className="focus:shadow-outline relative z-10 w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none dark:border-stone-600 dark:bg-stone-700 dark:text-white"
                id="message"
                placeholder={t("messagePlaceholder")}
              />
            </label>
          </div>
          <div className="relative z-10 col-span-2 mt-4 sm:mt-0">
            <label
              className="mb-2 block flex gap-4 text-sm font-bold text-gray-700 dark:text-white"
              htmlFor="human"
            >
              <input
                checked={human}
                onChange={() => setHuman(!human)}
                className="h-6 w-6 appearance-none rounded border-2 border-purple-600 bg-gray-100 accent-purple-600 checked:appearance-auto"
                id="human"
                type="checkbox"
              />
              <span>{t("human")}</span>
            </label>
          </div>
          <div className="relative z-10 col-span-2 mt-4 sm:mt-0">
            <label
              className="mb-2 block flex gap-4 text-sm font-bold text-gray-700 dark:text-white"
              htmlFor="robot"
            >
              <input
                checked={robot}
                onChange={() => setRobot(!robot)}
                className="h-6 w-6 appearance-none rounded border-2 border-purple-600 bg-gray-100 accent-purple-600 checked:appearance-auto"
                id="robot"
                type="checkbox"
              />
              <span>{t("robot")}</span>
            </label>
          </div>
        </div>
        <button
          onClick={(e) => handleSubmit(e)}
          type="submit"
          className="relative z-10 col-span-2 mt-4 inline-flex items-center rounded-md bg-gradient-to-r from-purple-600 to-pink-600 px-3 py-2 text-sm font-semibold text-white shadow sm:mt-0"
        >
          <IconSend2 className="mr-2" size={18} />
          <span>{t("send")}</span>
        </button>
      </form>
      <div
        className={`${
          isOpen ? "block" : "hidden"
        } fixed bottom-4 right-4 z-50 w-96 max-w-[calc(100vw-32px)] rounded-xl bg-white p-4 shadow transition`}
      >
        <p className="text-lg font-semibold text-purple-600">{title}</p>
        <p>{msg}</p>
      </div>
    </>
  );
}
