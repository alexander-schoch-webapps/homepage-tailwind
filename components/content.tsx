import { ReactNode } from "react";

export default function Content({
  children,
  showLines = true,
  fullWidth = false,
}: {
  children: ReactNode | ReactNode[];
  showLines?: boolean;
  fullWidth?: boolean;
}) {
  return (
    <div className="relative flex flex grow justify-center border-t border-purple-200 bg-stone-50 px-4 pb-4 dark:border-stone-700 dark:bg-stone-800">
      {showLines && (
        <div className="absolute hidden h-full w-[80rem] max-w-full sm:block">
          <div
            className="absolute right-24 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
            style={{
              backgroundSize: "2px 13px",
              backgroundImage:
                "linear-gradient(rgb(147, 51, 234) 61%,rgba(255,255,255,0) 0%)",
            }}
          />
          <div
            className="absolute right-32 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
            style={{
              backgroundSize: "2px 13px",
              backgroundImage:
                "linear-gradient(rgb(147, 51, 234) 61%,rgba(255,255,255,0) 0%)",
            }}
          />
        </div>
      )}
      <div
        className={`mx-auto max-w-full pt-16 ${fullWidth ? "w-full" : "w-[80rem]"}`}
      >
        {children}
      </div>
    </div>
  );
}
