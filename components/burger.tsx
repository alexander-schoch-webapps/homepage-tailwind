"use client";

import Link from "next/link";

import { useState } from "react";
import type { ReactNode } from "react";

import { Dialog } from "@headlessui/react";

import {
  IconMenu2,
  IconX,
  IconCode,
  IconHome,
  IconBriefcase2,
  IconTool,
  IconWriting,
  IconMail,
} from "@tabler/icons-react";

import LanguageSwitcherMobile from "@/components/languageSwitcherMobile";

interface NavItemType {
  path: string;
  label: string;
}

interface IconType {
  [index: string]: ReactNode;
}

export default function Burger({
  navItems,
  locale,
}: {
  navItems: NavItemType[];
  locale: "de" | "en";
}) {
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const icons = {
    "/": <IconHome />,
    "/cv": <IconBriefcase2 />,
    "/projects": <IconTool />,
    "https://blog.aschoch.ch": <IconWriting />,
    "/contact": <IconMail />,
  } as IconType;

  return (
    <>
      <button onClick={() => setShowMenu(true)}>
        <IconMenu2 className="my-auto" size={24} />
      </button>
      <Dialog open={showMenu} onClose={() => setShowMenu(false)}>
        <div className="fixed inset-0 z-40 flex w-screen items-start justify-center bg-black/30 p-4 backdrop-blur-sm">
          <Dialog.Panel className="relative mt-16 w-full max-w-96 rounded-md border border-slate-200 bg-white p-4 shadow-xl dark:border-stone-600 dark:bg-stone-700">
            <div className="absolute right-4 top-4 z-50">
              <button onClick={() => setShowMenu(false)}>
                <IconX className="my-auto" size={24} />
              </button>
            </div>
            <ul className="list-none">
              {navItems.map((item) => (
                <li className="my-2" key={item.path}>
                  <Link
                    className="flex items-center gap-4 text-xl font-bold"
                    href={item.path}
                  >
                    {icons[item.path]}
                    {item.label}
                  </Link>
                </li>
              ))}
            </ul>
            <hr className="divide-solid" />
            <div className="mt-4 inline">
              <a
                href="https://gitlab.com/alexander-schoch-webapps/homepage-tailwind"
                target="_blank"
              >
                <IconCode
                  size={32}
                  className="inline text-purple-600 dark:text-white"
                />
              </a>
              <LanguageSwitcherMobile locale={locale} />
            </div>
          </Dialog.Panel>
        </div>
      </Dialog>
    </>
  );
}
