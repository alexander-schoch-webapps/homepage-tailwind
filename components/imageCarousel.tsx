"use client";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

import parse from "html-react-parser";

export default function ImageCarousel({
  images,
  locale,
  shorten,
}: {
  images: { path: string; caption: { de: string; en: string } }[];
  locale: "de" | "en";
  shorten: boolean;
}) {
  if (images.length === 0) return <></>;

  return (
    <Carousel swipeable={true} emulateTouch={true}>
      {images.map((image, i) => (
        <div className="grid grid-cols-1 gap-4 sm:grid-cols-2" key={i}>
          <div className="overflow-hidden rounded-xl border border-purple-200 shadow">
            <img
              src={`/images/projects/${image.path}`}
              alt={image.caption[locale]}
            />
          </div>
          <p className="my-auto text-left dark:text-white">
            <span className="mr-4 font-semibold text-purple-600 dark:text-white">
              Figure {i + 1}:
            </span>
            {parse(image.caption[locale])}
          </p>
        </div>
      ))}
    </Carousel>
  );
}
