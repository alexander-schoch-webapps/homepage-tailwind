import Link from "next/link";

import { ReactNode } from "react";

import { IconCode } from "@tabler/icons-react";

import Burger from "@/components/burger";
import LanguageSwitcherMobile from "@/components/languageSwitcherMobile";

import translate from "@/locales/translate";

export default function Navbar({
  children,
  locale,
}: {
  children: ReactNode | ReactNode[];
  locale: "de" | "en";
}) {
  const t = translate(locale);

  const links = [
    {
      label: t("home"),
      path: "/",
    },
    {
      label: t("cv"),
      path: "/cv",
    },
    {
      label: t("projects"),
      path: "/projects",
    },
    {
      label: t("blog"),
      path: "https://blog.aschoch.ch",
    },
    {
      label: t("contactMe"),
      path: "/contact",
    },
  ];

  return (
    <div className="flex min-h-screen w-full flex-col content-center from-violet-900 to-pink-900 dark:bg-gradient-to-r">
      <nav className="fixed z-50 flex w-full flex-row justify-between bg-white bg-opacity-70 px-4 py-4 shadow-md backdrop-blur-lg backdrop-filter dark:bg-transparent dark:bg-opacity-70">
        <Link href="/">
          <p className="text-xl font-bold text-purple-600 lg:text-2xl dark:text-white">
            Alexander Schoch
          </p>
        </Link>
        <div className="flex">
          <div className="hidden lg:block">
            {links.map((link) => (
              <Link
                key={link.path}
                className="px-4 font-bold text-stone-600 dark:text-stone-100"
                href={link.path}
              >
                {link.label}
              </Link>
            ))}
            <div className="inline border-l border-stone-300 pl-4">
              <a
                href="https://gitlab.com/alexander-schoch-webapps/homepage-tailwind"
                target="_blank"
                aria-label="Source Code"
              >
                <IconCode
                  size={24}
                  className="inline text-gray-600 dark:text-stone-100"
                />
              </a>
              <LanguageSwitcherMobile locale={locale} />
            </div>
          </div>
          <div className="block flex content-center lg:hidden">
            <Burger navItems={links} locale={locale} />
          </div>
        </div>
      </nav>
      <main className="mt-16 flex grow text-sm leading-6 text-black sm:text-base sm:leading-7">
        {children}
      </main>
    </div>
  );
}
