"use client";

import { usePathname } from "next/navigation";

export default function LanguageSwitcher({ locale }: { locale: "de" | "en" }) {
  const locales = ["de", "en"];
  const newLocale = locales.filter((l) => l != locale)[0];

  const fullPath = usePathname();

  if (!fullPath) return <></>;

  const path = fullPath.substring(3);

  const url = `/${newLocale}${path}`;
  return (
    <a href={url}>
      <button className="relative z-10 col-span-2 ml-4 mt-4 mt-4 inline-flex items-center rounded-md border bg-gradient-to-r from-purple-600  to-pink-600 px-2 py-1 text-sm font-semibold uppercase text-white shadow sm:mt-0 dark:bg-white dark:bg-none dark:text-purple-600">
        {newLocale}
      </button>
    </a>
  );
}
