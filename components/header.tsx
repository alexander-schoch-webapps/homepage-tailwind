import Link from "next/link";

import { IconChevronDown } from "@tabler/icons-react";

import descriptions from "@/content/header.json";

export default function Header({
  t,
  locale,
}: {
  t: (key: string) => string;
  locale: "de" | "en";
}) {
  return (
    <div>
      <div className="mx-auto flex h-[calc(100vh-4rem)] w-[80rem] max-w-full flex-col justify-evenly px-4 text-center">
        <div className="z-10">
          <p className="text-slate-5 mb-2 text-sm font-semibold text-stone-500 lg:text-lg dark:text-stone-300">
            {t("hey")}
          </p>
          <h1 className="bg-gradient-to-br from-violet-600 to-pink-600 bg-clip-text text-5xl font-black text-transparent sm:text-6xl md:text-7xl dark:text-white">
            Alexander Schoch
          </h1>
          <p className="mt-2 text-sm font-semibold text-stone-500 lg:text-lg dark:text-stone-300">
            {t("chemEng")}
          </p>
        </div>
        <div className="z-10 mx-auto motion-safe:animate-bounce">
          <Link href="/#content" aria-label="Go to main content">
            <IconChevronDown
              className="text-purple-600 dark:text-white"
              size={64}
            />
          </Link>
        </div>
        {/*<div
          className="bg-grid-slate-900 absolute inset-0 z-0 block h-screen dark:hidden"
          style={{
            backgroundSize: "20px 20px",
            backgroundImage:
              "radial-gradient(circle, #cccccc 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />
        <div
          className="bg-grid-slate-900 absolute inset-0 z-0 hidden h-screen dark:block"
          style={{
            backgroundSize: "20px 20px",
            backgroundImage:
              "radial-gradient(circle, #777777 1px, rgba(0, 0, 0, 0) 1px)",
            maskImage: "linear-gradient(to bottom, transparent, black)",
            WebkitMaskImage: "linear-gradient(to bottom, transparent, black)",
          }}
        />*/}
      </div>
    </div>
  );
}
