"use client";

import { useState, useEffect, ReactNode } from "react";

import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

const CarouselCard = ({
  k,
  i,
  content,
  colors,
  slide,
}: {
  k: string;
  i: number;
  content: ReactNode;
  colors: string[];
  slide: number;
}) => (
  <div
    className={`h-full w-full py-8 text-left duration-200 ${i !== slide ? "scale-90" : ""}`}
  >
    <div
      className={`flex border border-stone-200 bg-white text-black dark:border-stone-600 dark:bg-stone-700 dark:text-stone-100 ${i === slide ? "shadow-lg" : "shadow-sm"} h-full flex-col justify-start rounded-lg p-4`}
    >
      <h3 className={`font-black ${colors[i]} mb-4 text-3xl sm:text-4xl`}>
        {k[0].toUpperCase() + k.slice(1)}
      </h3>
      {content}
    </div>
  </div>
);

export default function TaglineCarousel({
  slide,
  setSlide,
  content,
  colors,
  keys,
}: {
  slide: number;
  setSlide: (i: number) => void;
  content: (key: string, color: string) => ReactNode;
  colors: string[];
  keys: string[];
}) {
  console.log(slide);
  return (
    <>
      <Carousel
        className="flex hidden flex-col overflow-visible sm:block"
        selectedItem={slide}
        autoPlay={false}
        centerMode={true}
        centerSlidePercentage={33}
        infiniteLoop={true}
        showStatus={false}
        showIndicators={false}
        onChange={(i) => setSlide(i)}
        showThumbs={false}
      >
        {keys.map((k, i) => (
          <CarouselCard
            k={k}
            i={i}
            key={k}
            content={content(keys[i], colors[i])}
            colors={colors}
            slide={slide}
          />
        ))}
      </Carousel>
      <Carousel
        className="flex flex-col overflow-visible sm:hidden"
        selectedItem={slide}
        autoPlay={false}
        centerMode={true}
        centerSlidePercentage={100}
        infiniteLoop={true}
        showStatus={false}
        showIndicators={false}
        onChange={(i) => setSlide(i)}
        showThumbs={false}
      >
        {keys.map((k, i) => (
          <CarouselCard
            k={k}
            i={i}
            key={k}
            content={content(keys[i], colors[i])}
            colors={colors}
            slide={slide}
          />
        ))}
      </Carousel>
    </>
  );
}
