import Link from "next/link";

import parse from "html-react-parser";

import {
  IconExternalLink,
  IconArrowRight,
  IconArrowLeft,
} from "@tabler/icons-react";

import ImageCarousel from "@/components/imageCarousel";

export default function Project({
  project,
  shorten,
  locale,
  t,
}: {
  project: {
    slug: string;
    teaser_image?: number;
    title: { de: string; en: string };
    type: { de: string; en: string };
    context?: { de: string; en: string };
    figures: { path: string; caption: { de: string; en: string } }[];
    text: { de: string; en: string }[];
    links?: { label: { de: string; en: string }; url: string }[];
  };
  shorten: boolean;
  locale: "de" | "en";
  t: (key: string) => string;
}) {
  return (
    <div>
      <div className="mb-8">
        {shorten && (
          <div>
            <h2 className="inline text-4xl font-black text-purple-600 dark:text-white">
              {parse(project.title[locale])}
            </h2>
            <p className="mb-4 text-lg font-semibold text-slate-400 dark:text-stone-300">
              {parse(project.type[locale])}
            </p>
          </div>
        )}

        {project.figures.length > 0 && (
          <>
            {shorten ? (
              <div className="mb-8 grid grid-cols-1 gap-4 sm:grid-cols-2">
                <div className="overflow-hidden rounded-xl border border-purple-200 shadow">
                  <img
                    src={`/images/projects/${
                      project.figures[project.teaser_image || 0].path
                    }`}
                  />
                </div>
                <p className="my-auto text-left dark:text-white">
                  <span className="mr-4 font-semibold text-purple-600 dark:text-white">
                    Figure {(project.teaser_image || 0) + 1}:
                  </span>
                  {parse(
                    project.figures[project.teaser_image || 0].caption[locale],
                  )}
                </p>
              </div>
            ) : (
              <ImageCarousel
                images={project.figures}
                locale={locale}
                shorten={shorten}
              />
            )}
          </>
        )}

        {project.context && (
          <p className="mb-4 dark:text-white">
            <i>{parse(project.context[locale])}</i>
          </p>
        )}

        {shorten ? (
          <p className="dark:text-white">{parse(project.text[0][locale])}</p>
        ) : (
          <>
            {project.text.map((txt, i) => (
              <p key={i} className="my-2 dark:text-white">
                {parse(txt[locale])}
              </p>
            ))}
          </>
        )}

        <div className="flex gap-2">
          {shorten ? (
            <Link href={`/project/${project.slug}`}>
              <button className="mr-2 mt-3 inline-flex items-center rounded-md bg-gradient-to-r from-purple-600 to-pink-600 px-3 py-2 text-sm font-semibold text-white shadow">
                <IconArrowRight className="mr-2" size={18} />
                {t("readMore")}
              </button>
            </Link>
          ) : (
            <Link href={`/projects`}>
              <button className="mr-2 mt-3 inline-flex items-center rounded-md bg-purple-600 px-3 py-2 text-sm font-semibold text-white shadow">
                <IconArrowLeft className="mr-2" size={18} />
                {t("goBack")}
              </button>
            </Link>
          )}

          {!shorten && project.links && project.links.length && (
            <div>
              {project.links.map((link) => (
                <a href={link.url} target="_blank" key={link.url}>
                  <button className="mr-2 mt-3 inline-flex items-center rounded-md bg-purple-600 px-3 py-2 text-sm font-semibold text-white shadow">
                    <IconExternalLink className="mr-2" size={18} />
                    {link.label[locale]}
                  </button>
                </a>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
