import {
  IconRotateRectangle,
  IconMessage2,
  IconStar,
  IconArrowRight,
} from "@tabler/icons-react";

import translate from "@/locales/translate";

interface PostType {
  reblog: ReblogType | null;
  account: AccountType;
  content: string;
  replies_count: number;
  reblogs_count: number;
  favourites_count: number;
  media_attachments: ImageType[];
  card: CardType;
}

interface ReblogType {
  account: AccountType;
  content: string;
  media_attachments: ImageType[];
}

interface AccountType {
  url: string;
  avatar_static: string;
  display_name: string;
  acct: string;
  emojis: EmojiType[];
}

interface EmojiType {
  static_url: string;
}

interface ImageType {
  url: string;
}

interface CardType {
  url: string;
  image: string;
  title: string;
  description: string;
}

export default async function Mastodon({ locale }: { locale: string }) {
  const t = translate(locale);

  const URL =
    "https://linuxrocks.online/api/v1/accounts/109301286257614288/statuses?exclude_replies=true";
  const data = await fetch(URL, {
    next: {
      revalidate: 3600,
    },
  });
  const posts_full = await data.json();
  const posts = posts_full.slice(0, 10);

  const getAccount = (data: PostType): AccountType => {
    if (data.reblog !== null) return data.reblog.account;
    else return data.account;
  };

  const getContent = (data: PostType): string => {
    if (data.reblog !== null) return data.reblog.content;
    else return data.content;
  };

  const getImages = (data: PostType): ImageType[] => {
    if (data.reblog !== null) return data.reblog.media_attachments;
    else return data.media_attachments;
  };

  const remove_badges = (name: string): string => {
    return name.replace(/:.*:/, "");
  };

  return (
    <div className="relative z-10 flex w-full flex-row gap-4 overflow-x-auto py-4">
      {posts.map((post: PostType, i: number) => {
        const account = getAccount(post);
        const content = getContent(post);
        const images = getImages(post);

        const interactions = [
          {
            count: post.replies_count,
            icon: <IconMessage2 size={20} />,
          },
          {
            count: post.reblogs_count,
            icon: <IconRotateRectangle size={20} />,
          },
          {
            count: post.favourites_count,
            icon: <IconStar size={20} />,
          },
        ];

        return (
          <div
            key={i}
            className="h-[32rem] w-full max-w-96 shrink-0 overflow-y-auto rounded-xl border border-purple-200 bg-white p-4 shadow-md dark:border-stone-600 dark:bg-stone-700"
          >
            <div className="h-8">
              {post.reblog && (
                <p className="flex items-center gap-2 text-sm font-semibold text-stone-400">
                  <IconRotateRectangle size={20} />
                  <span>
                    {post.account.display_name} {t("boosted")}
                  </span>
                </p>
              )}
            </div>
            <a
              href={account.url}
              className="flex items-center gap-2"
              target="_blank"
            >
              <img
                src={account.avatar_static}
                className="h-[46px] w-[46px] rounded-md"
              />
              <div>
                <p className="m-0 flex items-center gap-1 font-semibold text-black dark:text-white">
                  <span>{remove_badges(account.display_name)}</span>
                  {account.emojis.map((e, j) => (
                    <img src={e.static_url} className="h-4 w-4" key={j} />
                  ))}
                </p>
                <p className="text-sm text-stone-400">
                  <span>@{account.acct}</span>
                </p>
              </div>
            </a>

            <div className="mt-4 overflow-y-auto text-stone-800 dark:text-stone-300">
              <p dangerouslySetInnerHTML={{ __html: content }} />
            </div>

            {post.card && (
              <div className="mt-2 overflow-hidden rounded-md border border-stone-200 dark:border-stone-600">
                <a href={post.card.url} target="_blank">
                  <img src={post.card.image} />
                  <div className="p-2">
                    <p className="text-xs text-stone-400">{post.card.url}</p>
                    <p className="my-1 text-sm font-semibold text-stone-600 dark:text-stone-400">
                      {post.card.title}
                    </p>
                    <p className="text-xs text-stone-600 dark:text-stone-400">
                      {post.card.description}
                    </p>
                  </div>
                </a>
              </div>
            )}

            {images.map((img, j) => (
              <div className="mt-2 overflow-hidden rounded-md" key={j}>
                <img src={img.url} />
              </div>
            ))}

            {post.reblog === null && (
              <div className="mt-4 flex gap-6 text-sm font-semibold text-stone-400">
                {interactions.map((int, j) => (
                  <span className="flex items-center gap-2" key={j}>
                    {int.icon}
                    {int.count}
                  </span>
                ))}
              </div>
            )}
          </div>
        );
      })}

      <div className="flex items-center">
        <a
          className="flex items-center gap-2 whitespace-nowrap rounded-md bg-purple-700 px-3 py-2 font-semibold text-white shadow-md"
          href="https://linuxrocks.online/@alexander_schoch"
          target="_blank"
        >
          {t("readMore")} <IconArrowRight />
        </a>
      </div>
    </div>
  );
}
