import { ReactNode } from "react";

export default function Title({
  children: title,
  subtitle = "",
}: {
  children: string;
  subtitle?: string | ReactNode | ReactNode[];
}) {
  return (
    <div className="mx-auto flex h-80 w-[80rem] max-w-full flex-col justify-evenly text-center">
      <div className="z-10">
        <h1 className="bg-gradient-to-br from-violet-600 to-pink-600 bg-clip-text py-2 text-5xl font-black text-transparent sm:text-6xl md:text-7xl dark:text-white">
          {title}
        </h1>
        {subtitle && (
          <p className="text-lg font-semibold text-slate-500">{subtitle}</p>
        )}
      </div>
    </div>
  );
}
