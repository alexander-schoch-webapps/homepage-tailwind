import Link from "next/link";
import { headers, type UnsafeUnwrappedHeaders } from "next/headers";

export default function LanguageSwitcher({ locale }: { locale: "de" | "en" }) {
  const locales = ["de", "en"];
  const newLocale = locales.filter((l) => l != locale)[0];

  const headersList = headers() as unknown as UnsafeUnwrappedHeaders;
  const fullPath = headersList.get("x-pathname") || "";

  if (!fullPath) return <></>;

  const path = fullPath.substring(3);

  const url = `/${newLocale}${path}`;
  return (
    <Link href={url}>
      <button className="relative z-10 col-span-2 ml-4 mt-4 mt-4 inline-flex items-center rounded-md px-2 py-1 text-sm font-semibold uppercase shadow sm:mt-0">
        {newLocale}
      </button>
    </Link>
  );
}
