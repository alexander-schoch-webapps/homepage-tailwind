import descriptions from "@/content/header.json";

export default function About({
  t,
  locale,
}: {
  t: (key: string) => string;
  locale: "de" | "en";
}) {
  return (
    <div className="relative flex flex grow justify-center border-t border-pink-200 bg-pink-50 px-4 pb-4 dark:border-stone-700 dark:bg-stone-800">
      <div className="absolute hidden h-full w-[80rem] max-w-full sm:block">
        <div
          className="absolute right-24 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
          style={{
            backgroundSize: "2px 13px",
            backgroundImage:
              "linear-gradient(rgb(219, 39, 119) 61%,rgba(255,255,255,0) 0%)",
          }}
        />
        <div
          className="absolute right-32 top-0 z-0 h-full w-[2px] bg-right bg-repeat-y"
          style={{
            backgroundSize: "2px 13px",
            backgroundImage:
              "linear-gradient(rgb(219, 39, 119) 61%,rgba(255,255,255,0) 0%)",
          }}
        />
      </div>
      <div className="mx-auto w-[80rem] max-w-full py-16">
        <div className="mb-8">
          <h2 className="inline text-4xl font-black text-pink-600">
            {t("about")}
          </h2>
        </div>
        <div className="relative z-20 flex grid grid-cols-1 flex-col truncate rounded-3xl bg-white shadow-xl lg:aspect-[3/1] lg:grid-cols-3">
          <div>
            <img src="/images/photo.png" alt="Portrait" />
          </div>
          <div className="relative h-96 lg:col-span-2 lg:h-full">
            <div className="absolute h-full max-h-full bg-white p-4 text-left text-center lg:text-left dark:bg-stone-700">
              <div className="z-10 my-auto flex h-full flex-col justify-items-center overflow-y-scroll">
                <div className="my-auto h-fit text-left">
                  {descriptions.map((desc, i) => (
                    <p
                      key={i}
                      className="text-md my-2 whitespace-normal text-black dark:text-stone-100"
                    >
                      {desc[locale]}
                    </p>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
