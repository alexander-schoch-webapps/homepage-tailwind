import trans_de from "@/locales/de";
import trans_en from "@/locales/en";

export default function translate(locale) {
  const dict = locale === "de" ? trans_de : trans_en;
  return (key) => {
    if (!Object.keys(dict).includes(key)) return key;
    return dict[key];
  };
}
