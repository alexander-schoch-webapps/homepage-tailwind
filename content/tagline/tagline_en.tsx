import { ReactNode } from "react";

interface ContentType {
  [index: string]: ReactNode;
}

export default (key: string, color: string) => {
  const content = {
    "user-friendly": (
      <>
        <p className="my-2">
          Webapps need to be user-friendly, such that users have an intuitive
          understanding on how to interact with the appliation without needing
          to consult external documentation.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://react.dev"
          >
            React.js
          </a>{" "}
          within{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org"
          >
            Next.js
          </a>{" "}
          Client Components to provide instantaneous user feedback.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://next-international.vercel.app"
          >
            Next-International
          </a>{" "}
          to localize the application and giving the user the option to read its
          content in their preferred language.
        </p>
        <p className="my-2">
          I make sure that my applications always look good on any device.
        </p>
      </>
    ),
    beautiful: (
      <>
        <p className="my-2">
          Webapps need to be beautiful to make sure users like to interact with
          it.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://mantine.dev/"
          >
            Mantine
          </a>{" "}
          for accessible and beautifully looking pre-made components.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://tailwindcss.com"
          >
            Tailwind
          </a>{" "}
          to style anything exactly the way I want, while also allowing for
          server-side rendering.
        </p>
      </>
    ),
    fast: (
      <>
        <p className="my-2">
          Webapps need to be fast, such that users with slow internet, for
          example while on a train, can still enjoy it whitout having to wait
          for multiple minutes for a page to load.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            {" "}
            Next.js{" "}
          </a>{" "}
          Server Components to run most JavaScript code on the server, resulting
          in a lower bundle size and thus faster page loads.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            Next.js
          </a>{" "}
          to pre-render pages, which allows users to switch pages with
          near-instantaneous feedback.
        </p>
      </>
    ),
    secure: (
      <>
        <p className="my-2">
          Webapps need to be privacy-respecting and secure in order to protect
          its users data.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://authjs.dev/"
          >
            AuthJS
          </a>{" "}
          for user authentication, allowing for external identity providers or a
          secure local password database.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://prisma.io/"
          >
            Prisma
          </a>{" "}
          to run queries against a database without ever touching SQL. This
          approach makes sure that inputs are always sanitized and no SQL
          injections can occur.
        </p>
        <p className="my-2">
          I make sure that my applications never call any server that do not
          respect the user's privacy (e.g. Google Fonts or Google Analytics).
        </p>
      </>
    ),
    accessible: (
      <>
        <p className="my-2">
          Webapps need to be usable by any user, especially if they are disabled
          in some way. This can range from blindness <i>via</i> color vision
          deficiency to a broken arm from skiing and thus only being able to use
          the keyboard.
        </p>
        <p className="my-2">
          I use the{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.a11yproject.com/"
          >
            {" "}
            A11Y Project{" "}
          </a>{" "}
          to check if my applications meet a good standard for accessibility and
          implement its guidelines.
        </p>
      </>
    ),
    reliable: (
      <>
        <p className="my-2">
          Webapps need to be reliable to make sure they don't randomly crash or
          do stuff they're not supposed to do.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.typescriptlang.org/"
          >
            TypeScript
          </a>{" "}
          to find bugs at build-time instead of whenever some user encounters it
          on production.
        </p>
        <p className="my-2">
          I use{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://docker.com/"
          >
            Docker
          </a>{" "}
          to make sure that the application has the same environment on any
          server and that it is independent on the server's installed packages
          or its configuration.
        </p>
      </>
    ),
    maintainable: (
      <>
        <p className="my-2">
          Webapps need to be maintainable, such that it can be used for many
          years, even if the only person proficient with the code is not there
          anymore.
        </p>
        <p className="my-2">
          I only use well-documented and popular technologies that have a low
          entry barrier and flat learning curve, such as{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            Next.js
          </a>
          ,{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://react.dev/"
          >
            React.js
          </a>{" "}
          or{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://mantine.dev/"
          >
            Mantine
          </a>
          .
        </p>
        <p className="my-2">
          I make sure my applications are well-documented, such that users and
          developers find their way quickly into the project.
        </p>
        <p className="my-2">
          Anyone with some experience in programming should be able to pick up
          the project and start developing within a few hours.
        </p>
      </>
    ),
    "open source": (
      <>
        <p className="my-2">
          Webapps should be open source, such that any user can verify that the
          application does what it promises to do. The applications I write are
          completely built on open source projects, and I think it is thus my
          moral obligation to give whatever I do with them back to the
          community.
        </p>
        <p className="my-2">
          I usually license my webapps under the{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.tldrlegal.com/license/gnu-affero-general-public-license-v3-agpl-3-0"
          >
            GNU AGPL 3.0
          </a>{" "}
          license to make sure that any derivative of my work also needs to be
          free and open software.
        </p>
      </>
    ),
  } as ContentType;

  return content[key] || <></>;
};
