import { ReactNode } from "react";

interface ContentType {
  [index: string]: ReactNode;
}

export default (key: string, color: string) => {
  const content = {
    benutzerfreundlich: (
      <>
        <p className="my-2">
          Webapps müssen benutzerfreundlich sein, sodass User intuitiv
          verstehen, wie sie mit der Anwendung interagieren können, ohne eine
          externe Dokumentation konsultieren zu müssen.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://react.dev"
          >
            React.js
          </a>{" "}
          innerhalb von{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org"
          >
            Next.js
          </a>{" "}
          Client Components, um den Benutzer:innen instantanes Feedback zu
          geben.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://next-international.vercel.app"
          >
            Next-International
          </a>
          , um die Applikation zu lokalisieren und den Benutzer:innen die
          Möglichkeit zu bieten, den Inhalt in ihrer bevorzugten Sprache zu
          lesen.
        </p>
        <p className="my-2">
          Ich sorge dafür, dass meine Anwendungen auf allen Geräten stets gut
          aussehen.
        </p>
      </>
    ),
    hübsch: (
      <>
        <p className="my-2">
          Webapps müssen hübsch sein, damit Benutzer:innen gerne mit ihnen
          interagieren.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://mantine.dev/"
          >
            Mantine
          </a>{" "}
          für leicht zugängliche und schön aussehende vorgefertigte Komponenten.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://tailwindcss.com"
          >
            Tailwind
          </a>
          , um alles genau so zu gestalten, wie ich es möchte und gleichzeitig
          Server-Side Rendering zu ermöglichen.
        </p>
      </>
    ),
    schnell: (
      <>
        <p className="my-2">
          Webanwendungen müssen so schnell sein, dass Nutzer mit langsamem
          Internet, z.B. im Zug, sie trotzdem nutzen können, ohne mehrere
          Minuten auf das Laden einer Seite warten zu müssen.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            Next.js
          </a>
          , um den Grossteil des JavaScript-Codes auf dem Server auszuführen,
          was zu einer geringeren Grösse des Pakets und damit zu schnelleren
          Seitenladezeiten führt.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            Next.js
          </a>
          , um Seiten vorzurendern, was es den Benutzern ermöglicht, die Seiten
          mit nahezu sofortiger Rückmeldung zu wechseln.
        </p>
      </>
    ),
    sicher: (
      <>
        <p className="my-2">
          Webapps müssen die Privatsphäre respektieren und sicher sein, um die
          Daten ihrer Nutzer zu schützen.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://authjs.dev/"
          >
            AuthJS
          </a>{" "}
          für die Benutzerauthentifizierung, das externe Identitätsanbieter oder
          eine sichere lokale Passwortdatenbank ermöglicht.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://prisma.io/"
          >
            Prisma
          </a>
          , um Abfragen gegen Datenbankn auszuführen, ohne jemals SQL zu
          berühren. Dieser Ansatz stellt sicher, dass die Eingaben immer
          bereinigt werden und keine SQL-Injektionen auftreten können.
        </p>
        <p className="my-2">
          Ich stelle sicher, dass meine Anwendungen keine Server aufrufen, die
          die Privatsphäre der Nutzer nicht respektieren (z. B. Google Fonts
          oder Google Analytics).
        </p>
      </>
    ),
    barrierefrei: (
      <>
        <p className="my-2">
          Webapps müssen für alle User benutzbar sein, besonders wenn sie in
          irgendeiner Weise behindert sind. Das kann von Blindheit über
          Farbsehschwäche bis hin zu einem gebrochenen Arm beim Skifahren
          reichen, sodass nur die Tastatur benutzt werden kann.
        </p>
        <p className="my-2">
          Ich verwende das{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.a11yproject.com/"
          >
            {" "}
            A11Y Projekt{" "}
          </a>
          , um zu prüfen, ob meine Anwendungen einem guten Standard für
          Barrierefreiheit entsprechen und die entsprechenden Leitlinien
          umzusetzen.
        </p>
      </>
    ),
    zuverlässig: (
      <>
        <p className="my-2">
          Webanwendungen müssen zuverlässig sein, um sicherzustellen, dass sie
          nicht zufällig abstürzen oder Dinge tun, die sie nicht tun sollen.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.typescriptlang.org/"
          >
            TypeScript
          </a>
          , um Bugs zum Zeitpunkt des Kompilierens zu finden und nicht erst,
          wenn ein Benutzer in der Produktion darauf stösst.
        </p>
        <p className="my-2">
          Ich verwende{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://docker.com/"
          >
            Docker
          </a>
          , um sicherzustellen, dass die Anwendung auf jedem Server die gleiche
          Umgebung hat und dass sie unabhängig von den installierten Paketen
          oder der Konfiguration des Servers ist.
        </p>
      </>
    ),
    wartbar: (
      <>
        <p className="my-2">
          Webanwendungen müssen wartbar sein, so dass sie über viele Jahre
          hinweg genutzt werden können, auch wenn die einzige Person, die den
          Code beherrscht, nicht mehr da ist.
        </p>
        <p className="my-2">
          Ich verwende nur gut dokumentierte und weit verbreitete Technologien,
          die eine niedrige Einstiegshürde und eine flache Lernkurve haben, wie
          z. B.{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://nextjs.org/"
          >
            Next.js
          </a>
          ,{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://react.dev/"
          >
            React.js
          </a>{" "}
          oder{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://mantine.dev/"
          >
            Mantine
          </a>
          .
        </p>
        <p className="my-2">
          Ich achte darauf, dass meine Anwendungen gut dokumentiert sind, damit
          Benutzer und Entwickler sich schnell in das Projekt einfinden können.
        </p>
        <p className="my-2">
          Jede:r, der/die ein wenig Erfahrung im Programmieren hat, sollte in
          der Lage sein, das Projekt zu übernehmen und innerhalb weniger Stunden
          mit der Entwicklung zu beginnen.
        </p>
      </>
    ),
    quelloffen: (
      <>
        <p className="my-2">
          Webapps sollten Open Source sein, so dass jede:r Benutzer:in
          überprüfen kann, ob die Anwendung das tut, was sie zu tun verspricht.
          Die Anwendungen, die ich schreibe, basieren vollständig auf
          Open-Source-Projekten, und ich halte es daher für meine moralische
          Pflicht, alles, was ich mit ihnen mache, an die Community
          zurückzugeben.
        </p>
        <p className="my-2">
          Ich lizensiere meine Applikationen typischerweise unter der{" "}
          <a
            className={`${color} font-semibold`}
            target="_blank"
            href="https://www.tldrlegal.com/license/gnu-affero-general-public-license-v3-agpl-3-0"
          >
            GNU AGPL 3.0
          </a>
          , um sicherzustellen, dass alle Derivate ebenfalls Frei und Open
          Source sein müssen.
        </p>
      </>
    ),
  } as ContentType;

  return content[key] || <></>;
};
