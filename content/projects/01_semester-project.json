{
  "slug": "nox-to-ammonia",
  "teaser_image": 3,
  "title": {
    "de": "Prozesssimulation und techno-ökonomische Beurteilung von NO<sub><i>x</i></sub>-zu-Ammoniak in ammoniakbetriebenen Frachtschiffen",
    "en": "Process Simulation and techno-economic Assessment of NO<sub><i>x</i></sub>-to-Ammonia in Ammonia powered Container Ships"
  },
  "type": {
    "de": "Semesterprojekt",
    "en": "Semester Project"
  },
  "context": {
    "de": "Diese Arbeit wurde im Rahmen eines Semesterprojekts unter der Aufsicht von Margarita Athanasia Charalambous in der Forschungsgruppe von Prof. Dr. Gonzalo Guillén Gosalbez an der ETH Zürich durchgeführt.",
    "en": "This work was carried out as part of a semester project under the supervision of Margarita Athanasia Charalambous in the research group of Prof. Dr. Gonzalo Guillén Gosalbez at ETH Zurich."
  },
  "figures": [
    {
      "path": "01_semester-project/01_ammonia-prices.png",
      "caption": {
        "de": "Ammoniakpreise für graues, blaues und grünes Ammoniak, aufgeschlüsselt nach der Herkunft. Es ist sichtbar, dass Grünes Ammoniak signifikant teurer ist als die anderen Typen. Allerdings sind die Preise von blauem und grauem Ammoniak vergleichbar, obwohl blaues Ammoniak 85% der Emissionen einspart.",
        "en": "Ammonia prices for gray, blue and green ammonia, broken down by origin. It can be seen that green ammonia is significantly more expensive than the other types. However, the prices of blue and gray ammonia are comparable, although blue ammonia saves 85% of the emissions."
      }
    },
    {
      "path": "01_semester-project/02_setup.png",
      "caption": {
        "de": "Grober aufbau der Simulation. Ammoniak wird im Motor verbrannt und im Cracker zu Wasserstoff verarbeitet. Diese Produkte werden dann im NTA zu Ammoniak reagiert, und dieses kann dann wieder als Treibstoff verwendet werden.",
        "en": "Rough structure of the simulation. Ammonia is burned in the engine and processed into hydrogen in the cracker. These products are then reacted in the NTA to form ammonia, which can then be reused as fuel."
      }
    },
    {
      "path": "01_semester-project/03_capex-opex.png",
      "caption": {
        "de": "Auswertung von einmaligen Kosten (Abschreibung von Reaktoren, etc., CAPEX) und laufenden Kosten (Treibstoff, OPEX). Es ist sichtbar, dass die Treibstoffkosten klar überwiegen, und dass alle Ammoniakfälle im vergleich zum Dieselfall (BAU) teurer ausfallen.",
        "en": "Evaluation of one-off costs (depreciation of reactors, etc., CAPEX) and ongoing costs (fuel, OPEX). It can be seen that the fuel costs clearly predominate and that all ammonia cases are more expensive than the diesel case (BAU)."
      }
    },
    {
      "path": "01_semester-project/04_lca.png",
      "caption": {
        "de": "Auswertung der Ökobilanz (Life Cycle Assessement, LCA). Es ist sichbar, dass Ammoniak mit den Annahmen aus diesem Projekt trotz des fehlenden direkten CO<sub>2</sub>-Ausstosses auch bei der teuersten Ammoniakquelle einen grösseren Einfluss auf das Klima als ein Dieselschiff hat.",
        "en": "Life Cycle Assessement (LCA). It can be seen that, despite the lack of direct CO<sub>2</sub> emissions, ammonia has a greater impact on the climate than a diesel ship, even with the most expensive ammonia source, using the assumptions from this project."
      }
    }
  ],
  "text": [
    {
      "de": "Obwohl das Frachtschiff eines der effizientesten Methoden zum Warentransport ist, ist diese Industrie rein durch ihre Grösse trotzdem für 3% der globalen CO<sub>2</sub>-Emissionen verantwortlich. Entsprechend können alternative Treibstoffe, wie z.B. Ammoniak oder Methanol, diese Emissionen potentiell reduzieren. Konkret hat hier Ammoniak mit seiner vergleichsweise einfachen Handhabung und hohen Energiedichte viel Potential. Das Problem mit Ammonika ist allerdings, dass die Verbrennung dessen Stickoxide (NO<sub><i>x</i></sub>) freisetzt, welche über die N<sub>2</sub>O-Bildung indirekt zum Klimawandel beitragen. Somit ist das Ziel dieses Projekts, mithilfe der Software \"Aspen Plus V11\" zu untersuchen, ob es Sinn macht, hierfür einen NO<sub><i>x</i></sub>-zu-Ammoniak-Prozess (NTA) einzubauen.",
      "en": "Although the cargo ship is one of the most efficient methods of transporting goods, this industry is still responsible for 3% of global CO<sub>2</sub> emissions purely due to its size. Accordingly, alternative fuels, such as ammonia or methanol, can potentially reduce these emissions. Specifically, ammonia with its comparatively easy handling and high energy density has great potential here. The problem with ammonia, however, is that burning it releases nitrogen oxides (NO<sub><i>x</i></sub>), which indirectly contribute to climate change through the formation of N<sub>2</sub>O. The aim of this project is therefore to use the software \"Aspen Plus V11\" to investigate whether it makes sense to incorporate a NO<sub><i>x</i></sub>-to-ammonia (NTA) process for this purpose."
    },
    {
      "de": "Für alle Resultate wurden drei Ammoniakfälle mit dem \"Business as Usual\" (Diesel) Verglichen, weil der Preis und der CO<sub>2</sub>-Ausstoss je nach Produktionsprozess start variiert. Graues Ammoniak wird aus Stickstoff und Wasserstoff über den Haber-Bosch-Prozess hergestellt, wobei der Wasserstoff aus Methan und Wasser über Steam Reforming und Water Gas Shift hergestellt wird. Dieser Prozess stösst pro vier mol Wasserstoff auch immer ein mol CO<sub>2</sub> aus. Blaues Ammoniak wird gleich hergestellt, aber 85% des CO<sub>2</sub> wird mittels Carbon Capture and Storage abgefangen und verarbeitet. Grünes Ammoniak benutzt Wasserstoff aus Wasserelektrolyse mit grünem Strom. Die Preise für diese Ammoniakquellen werden in Abb. 1 verglichen.",
      "en": "For all results, three ammonia cases were compared with \"Business as Usual\" (diesel) because the price and CO<sub>2</sub> emissions vary depending on the production process. Gray ammonia is produced from nitrogen and hydrogen via the Haber-Bosch process, with the hydrogen being produced from methane and water via steam reforming and water gas shift. This process emits one mole of CO<sub>2</sub> for each mole of hydrogen. Blue ammonia is produced in the same way, but 85% of the CO<sub>2</sub> is captured and processed using Carbon Capture and Storage. Green ammonia uses hydrogen from water electrolysis using green electricity. The prices for these ammonia sources are compared in Fig. 1"
    },
    {
      "de": "Die Idee in diesem Prozess ist, das bei der Verbrennung entstandene Stickoxid mit Wasserstoff zurück zu Ammoniak und Wasser zu reagieren. Der Wasserstoff dazu wird in einem Cracker aus Ammoniak hergestellt (Siehe Abb. 2). Dieses kann dann über einen Separationsprozess von den Verbrennungsprodukten getrennt und wieder als Treibstoff verwendet werden. Mit den Resultaten wurde dann ausgewertet, ob sich der NTA-Prozess sowohl aus finanzieller als auch aus ökologischer Sicht lohnt.",
      "en": "The idea behind this process is to react the nitrogen oxide produced during combustion with hydrogen back to ammonia and water. The hydrogen for this is produced from ammonia in a cracker (see Fig. 2). This can then be separated from the combustion products in a separation process and reused as fuel. The results were then used to evaluate whether the NTA process is worthwhile from both a financial and an ecological point of view."
    },
    {
      "de": "Für die ökonomische Auswertung muss zunächst zwischen einmaligen Kosten für Equipment (Capital Expenditure, CAPEX) und laufenden Kosten (Operating Expenditure, OPEX) unterschieden werden. Das CAPEX wird hier auf eine Lebenszeit von 15 Jahren abgeschrieben und dann jährlich mit dem OPEX verglichen. In Abb. 3 ist sichtbar, dass in diesem Fall die OPEX, effektiv also die Treibstoffkosten, klar überwiegen, und die Kosten für ein Ammoniakschiff im Vergleich zu einem Dieselschiff (Business as Usual, BAU), wie erwartet, überwiegen. Grund dafür ist, dass, obwohl Ammoniak einen tieferen Kilogrammpreis als Diesel hat, der Treibstoffverbrauch bei Ammoniak viel höher als bei Diesel ist, weil wir auch noch den NTA-Reaktor mit Wasserstoff aus Ammoniak bespeisen müssen.",
      "en": "For the economic evaluation, a distinction must first be made between one-off costs for equipment (Capital Expenditure, CAPEX) and ongoing costs (Operating Expenditure, OPEX). The CAPEX is amortized over a lifetime of 15 years and then compared annually with the OPEX. Fig. 3 shows that in this case the OPEX, effectively the fuel costs, clearly outweigh the costs for an ammonia ship compared to a diesel ship (business as usual, BAU), as expected. The reason for this is that, although ammonia has a lower price per kilogram than diesel, the fuel consumption for ammonia is much higher than for diesel because we also have to feed the NTA reactor with hydrogen from ammonia."
    },
    {
      "de": "Die Auswertung der Ökobilanz kann in Abb. 4 eingesehen werden. Hier werden die Auswirkungen auf den Klimawandel von Ammoniak und Diesel verglichen. Hier wurde der Treibhauseffekt von Stickoxid (NO) aus der Literatur abgeschätzt, obwohl NO nur einen indirekten Einfluss auf den Klimawandel hat. Dies, kombiniert mit dem sehr hohen Treibstoffverbrauch von Ammoniak und der nur 90% Effizienz (also gelangen 10% des NO in die Atmosphäre) des NTA-Prozesses führt zu den Resultaten sichtbar in Abb. 4.",
      "en": "The evaluation of the life cycle assessment can be seen in Fig. 4. Here the effects on climate change of ammonia and diesel are compared. Here, the greenhouse effect of nitrogen oxide (NO) was estimated from the literature, although NO only has an indirect influence on climate change. This, combined with the very high fuel consumption of ammonia and the only 90% efficiency (i.e. 10% of the NO is released into the atmosphere) of the NTA process, leads to the results shown in Fig. 4."
    },
    {
      "de": "Es lässt sich zusammenfassen, dass sich mit dem Parametern aus diesem Projekt ein Ammoniakschiff noch nicht lohnt. Allerdings könnte sich dies mit einer höheren Effizienz des NTA-Prozesses und einer anderen Wasserstoffquelle in Zukunft ändern.",
      "en": "It can be summarized that with the parameters from this project, an ammonia ship is not yet worthwhile. However, this could change in the future with a higher efficiency of the NTA process and a different hydrogen source."
    }
  ],
  "links": [
    {
      "label": {
        "de": "Vollständiger Bericht (Englisch)",
        "en": "Complete Report"
      },
      "url": "https://gitlab.ethz.ch/schochal/semester-project/-/raw/main/report/tex/report.pdf?ref_type=heads&inline=false"
    }
  ]
}
