import semesterProject from "./01_semester-project.json";
import homepage from "./02_homepage.json";

const projects = [homepage, semesterProject];

export default projects;
